Weenotify
=========

Python script that works as both ``weechat plugin`` and ``notification server``.

As a weechat plugin, it can run in two modes:
  * ``local``: It will send notifications to libnotify locally
  * ``remote``: It will send notifications to the script running in server mode.

As a script, it can be ran in server mode where it listens to client notifications.
::
   
   $ python weenotify.py -s


Usage
-----

::

   $ python weenotify.py -h
   usage: weenotify.py [-h] [-s] [-H HOST] [-p PORT] [-V]

   Plugin/Server to send/receive notifications and display them

   optional arguments:
     -h, --help            show this help message and exit
     -s, --server          Run in server mode.
     -H HOST, --host HOST  The host/IP to bind to.
     -p PORT, --port PORT  The port to listen to.
     -V, --version         Prints version.


Dependencies
------------

As a plugin running in ``local`` mode, the dependencies are:
  * ``weechat`` (provided by weechat being installed)
  * ``dbus-python`` (required by notify2)
  * ``notify2``

As a plugin running in ``remote`` mode, the dependencies are:
  * ``weechat`` (provided by weechat being installed)

As a script running in ``server`` mode, the dependencies are:
  * ``dbus-python`` (required by notify2)
  * ``notify2``
